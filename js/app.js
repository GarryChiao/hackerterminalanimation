function stepOne () {
  var terminalElement = $(".terminal")[0];
  var dialogue	=	document.getElementsByClassName("dialogue")[0];
  1 || (function(o){
    var open = false;
    Object.defineProperty(o, "open", {
      get:	function(){ return o.classList.contains("open"); },
      set:	function(i){ o.classList.toggle("open", !!i); }
    });
  }(dialogue));
  function snakeText(from, to, refreshRate, charAmount, autoScroll){
    var fromText, toText, l, i,
        refreshRate		=	refreshRate	|| 20,
        charAmount		=	charAmount	|| 1;
    if(3 === from.nodeType) fromText = from;
    else for(i = l = from.childNodes.length-1; i >= 0; --i)
      if(3 === from.childNodes[i].nodeType){
        fromText = from.childNodes[i];
        break;
      }
    if(!fromText) throw new ArgumentError("Source object is neither a text node or element containing any text nodes.");
    if(3 === to.nodeType) toText = to;
    else for(i = l = to.childNodes.length-1; i >= 0; --i)
      if(3 === to.childNodes[i].nodeType){
        toText	=	to.childNodes[i];
        break;
      }

    toText	=	toText || to.appendChild(document.createTextNode(""));
    var interval = setInterval(function(){
      var from	=	fromText.data;
      if(!from.length) return clearInterval(interval);
      toText.data		+=	from.substr(0, charAmount);
      fromText.data	=	from.substr(charAmount);

      if(autoScroll && ($('.terminalwrapper').get(0).scrollTop > 20))
        console.log($('.terminalwrapper').get(0).scrollTop)
        $('.terminalwrapper').animate({
    scrollTop: $('.terminalwrapper').get(0).scrollHeight}, 0);
    }, refreshRate);
    return interval;
  }
  // var stringData = $.ajax({
  //     url: "https://textfiles.com/computers/2ndrs232.txt",
  //     async: false
  // }).responseText;

  var stringData  = '{"blockNumber":"6062039","timeStamp":"1533024744","hash":"0x1795851c7b777fcfc204953bf46b2a1cffaf5ded431a8052b80aebe7a94d47d2","nonce":"16","blockHash":"0x6dcd48d6aafbeb7d5a4e95ad15bebdb2730aec9332fa14bc587c081b5f0d7ea3","transactionIndex":"31","from":"0x7f2c265b503d72a89a156380276388215c8fc5a5","to":"0xdecf3a00e37bada548ec438dcef99b43d7f9f67d","value":"0","gas":"60000","gasPrice":"27558823936","isError":"0","txreceipt_status":"1","input":"0xa9059cbb00000000000000000000000004fd9fbf276cf06284c1f398a1b6805b2396b9760000000000000000000000000000000000000000000000878678326eac900000","contractAddress":"","cumulativeGasUsed":"1077553","gasUsed":"51950","confirmations":"261"},{"blockNumber":"6061909","timeStamp":"1533022818","hash":"0x31d1851ca60696ba86c3b1620d9cd4498e5c6bb4581d5d9a9f46faececef9b4d","nonce":"0","blockHash":"0xff5a79184b2eaff11fc4cbc4f51711f0435b364f0bfb5cfbc0d07bf916f2919e","transactionIndex":"99","from":"0x538e92346cfd5773f88b200b65119bc667fedb97","to":"0xdecf3a00e37bada548ec438dcef99b43d7f9f67d","value":"0","gas":"200000","gasPrice":"1010000000","isError":"0","txreceipt_status":"1","input":"0xa9059cbb000000000000000000000000965a53a52f4bc4fc5386ad8ed5430a3fe276d8ad000000000000000000000000000000000000000000000690758b54424dfc0000","contractAddress":"","cumulativeGasUsed":"4973926","gasUsed":"22014","confirmations":"391"},{"blockNumber":"6061765","timeStamp":"1533020618","hash":"0x291ca85bc4a73ce9ed9dd9aa77638fa9665143c3e5d4f1113f5055ebfac28789","nonce":"13","blockHash":"0xcbcfe097217d52a54d36c1704d127912417c01fa2e4c4df3e43de822d778a288","transactionIndex":"55","from":"0x5c49043ecdcf97583cdb81dd62d5c5533d7184f0","to":"0xdecf3a00e37bada548ec438dcef99b43d7f9f67d","value":"0","gas":"60000","gasPrice":"1000000000","isError":"0","txreceipt_status":"1","input":"0xa9059cbb000000000000000000000000538e92346cfd5773f88b200b65119bc667fedb97000000000000000000000000000000000000000000000690758b54424dfc0000","contractAddress":"","cumulativeGasUsed":"7180574","gasUsed":"37014","confirmations":"535"},{"blockNumber":"6061719","timeStamp":"1533019810","hash":"0x25e9dcd2dd1432dcbc96898da57e1463538417c3ef790b42b3dc8f56edcd3094","nonce":"7","blockHash":"0xd6961d73d1dd43f8ac96b7cd021826c3fb954bcf89f692d0ef611cac9e0c04ed","transactionIndex":"164","from":"0x1c66c42c1c8b982551e86912117eb0bf76e360d3","to":"0xdecf3a00e37bada548ec438dcef99b43d7f9f67d","value":"0","gas":"60000","gasPrice":"3000000000","isError":"0","txreceipt_status":"1","input":"0xa9059cbb000000000000000000000000df68c404fbb43eadfccced87e4a65882d949ca6e00000000000000000000000000000000000000000000000ad78ebc5ac6200000","contractAddress":"","cumulativeGasUsed":"5871947","gasUsed":"36950","confirmations":"581"}'

  snakeText(document.createTextNode(stringData), terminalElement, 10, 4, true);
}

$(".choose-lang-btn").click(function(){
  $(".choose-lang-wrapper").fadeOut(100);
  $(".term-demo-wrapper").fadeIn(100);
  $("#term_demo").fadeIn(100);
  // adding greeting text
  var i = 0;
  var greetingTxt = 'Lorem ipsum typing effect Lorem ipsum typing effect Lorem ipsum typing effect Lorem ipsum typing effect!'; /* The text */
  var speed = 50; /* The speed/duration of the effect in milliseconds */

  function typeWriter() {
      if (i < greetingTxt.length) {
          $('.terminal-output').append(greetingTxt.charAt(i))
          i++;
          setTimeout(typeWriter, speed);
      }
  }
  typeWriter()
})

stepOne()
setTimeout(function(){ 
  $(".slide-to-unlock-wrapper").fadeIn(100);
}, 8000);
